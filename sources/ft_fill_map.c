/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fill_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/19 14:10:41 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:13:15 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libft.h"
#include <stdlib.h>

static void	ft_fill_line(t_map **map, char *str, int i)
{
	int x;

	x = 0;
	while (*str)
	{
		if (ft_isdigit(*str) || *str == '-')
		{
			map[i][x].y = i;
			map[i][x].x = x;
			map[i][x++].z = ft_atoi(str);
			map[i][x - 1].color = 0xFFFFFF;
		}
		while (*str != ' ' && *str)
		{
			if (*str == ',')
				map[i][x - 1].color = ft_atoi_hex(str + 1);
			if (!*str)
				break ;
			++str;
		}
		if (!*str)
			break ;
		++str;
	}
}

t_map		**ft_fill_map(t_map **map, int fd)
{
	char	*line;
	char	*d;
	int		i;

	i = 0;
	line = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		if (line)
		{
			d = line;
			ft_fill_line(map, line, i);
			++i;
			free(d);
			line = NULL;
		}
	}
	if (line)
	{
		free(line);
		line = NULL;
	}
	return (map);
}
