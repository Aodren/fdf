/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_first_check.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 18:06:14 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 13:51:19 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static	int	ft_verif_hex(char **d)
{
	char *str;

	str = *d;
	++str;
	if (*str == '0' && (*(str + 1) == 'x' || *(str + 1) == 'X'))
	{
		str += 2;
		if (!*str)
			return (0);
		while (ft_isdigit(*str) || (*str >= 'a' && *str <= 'f')
				|| (*str >= 'A' && *str <= 'f'))
			++str;
		if (!*str || *str == ' ')
		{
			*d = str;
			return (1);
		}
		else
			return (0);
	}
	else
		return (0);
	*d = str;
	return (1);
}

static	int	ft_check_line(char *str)
{
	while (*str)
	{
		if (*str == '-')
			++str;
		if (!ft_isdigit(*str) && *str != ' ' && *str != '\t')
		{
			if (*str == ',')
			{
				if (!ft_verif_hex(&str))
					return (0);
				if (!*str)
					return (1);
			}
			else
				return (0);
		}
		++str;
	}
	return (1);
}

static void	ft_check_col(int *tab, char *str)
{
	while (*str)
	{
		if (*str == '-')
			++str;
		if (ft_isdigit(*str))
			++tab[1];
		while (*str != ' ' && *str)
			++str;
		if (!*str)
			break ;
		++str;
	}
}

static int	ft_free_check(char *d, char **line)
{
	free(d);
	*line = NULL;
	return (0);
}

int			ft_first_check(int fd, int *tab)
{
	char	*line;
	char	*d;
	int		tmpx;

	tab[0] = 0;
	tmpx = 0;
	line = NULL;
	while (get_next_line(fd, &line) == 1 && (d = line))
	{
		tab[1] = 0;
		if (!ft_check_line(line))
			return (ft_free_check(d, &line));
		ft_check_col(tab, line);
		if (tmpx == 0)
			tmpx = tab[1];
		else if (tab[1] - tmpx != 0)
			return (ft_free_check(d, &line));
		tab[0]++;
		ft_free_check(d, &line);
	}
	if (line)
		free(line);
	if (tab[0] == 0 || tab[1] == 0)
		return (0);
	return (1);
}
