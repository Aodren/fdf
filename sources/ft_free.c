/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/18 14:33:45 by abary             #+#    #+#             */
/*   Updated: 2016/03/18 14:52:26 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdlib.h>

static	void	ft_free_map(t_mlx *e, t_map **map)
{
	int	i;

	i = 0;
	while (i < e->ligne)
		free(map[i]);
	free (map);



}
void	ft_free_struct_mlx(t_mlx *e)
{
	if (e->map)
		ft_free_map(e, e->map);
	if (e->mapc)
		ft_free_map(e, e->mapc);


}
