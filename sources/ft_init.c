/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 19:55:09 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 13:42:43 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <math.h>
#include <stdlib.h>
#include "libft.h"

void	ft_init_coord(t_mlx *e)
{
	e->space = 0;
	e->maxx = 0;
	e->maxy = 0;
	e->maxz = 0;
	e->mapc = ft_init_map(e->ligne, e->colonne);
	e->angl_x = 30;
	e->angl_y = 30;
	e->angl_z = 0;
	e->decalx = 150;
	e->decaly = 150;
	e->z = 1.1;
	e->rotx = e->angl_x * M_PI / 180;
	e->roty = e->angl_y * M_PI / 180;
	e->rotz = e->angl_z * M_PI / 180;
}

t_map	**ft_init_map(int ligne, int col)
{
	t_map	**map;
	int		i;

	i = 0;
	if (!(map = malloc(sizeof(t_map *) * (ligne))))
		return (NULL);
	while (i < ligne)
	{
		if (!(map[i] = malloc(sizeof(t_map) * (col))))
			return (NULL);
		++i;
	}
	return (map);
}
