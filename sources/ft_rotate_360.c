/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotate_360.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/19 14:20:42 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:24:19 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "fdf.h"
#include <math.h>

void	ft_rotate_x_360(t_mlx *e)
{
	int rotate;

	rotate = 360;
	while (rotate > 0)
	{
		e->rotx = e->angl_x++ * M_PI / 180;
		ft_draw_map(e);
		mlx_do_sync(e->mlx);
		--rotate;
	}
}

void	ft_rotate_z_360(t_mlx *e)
{
	int rotate;

	rotate = 360;
	while (rotate > 0)
	{
		e->rotz = e->angl_z++ * M_PI / 180;
		ft_draw_map(e);
		mlx_do_sync(e->mlx);
		--rotate;
	}
}

void	ft_rotate_y_360(t_mlx *e)
{
	int rotate;

	rotate = 360;
	while (rotate > 0)
	{
		e->roty = e->angl_y++ * M_PI / 180;
		ft_draw_map(e);
		mlx_do_sync(e->mlx);
		--rotate;
	}
}
