/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fdf.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 21:04:54 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:18:04 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"
#include <stdlib.h>

int		ft_fdf(t_mlx *e)
{
	e->winx = WINX;
	e->winy = WINY;
	e->imgx = IMGX;
	e->imgy = IMGY;
	e->img = NULL;
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, e->winx, e->winy, "fdf");
	ft_init_coord(e);
	mlx_hook(e->win, 2, (1l << 0), ft_front, e);
	ft_draw_map(e);
	mlx_expose_hook(e->win, ft_draw_map, e);
	mlx_expose_hook(e->win, ft_draw_menu, e);
	mlx_do_sync(e->mlx);
	mlx_loop(e->mlx);
	return (1);
}
