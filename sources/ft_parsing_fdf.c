/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parsing_fdf.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 17:51:35 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 13:44:54 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <fcntl.h>
#include <unistd.h>
#include "libft.h"

t_map	**ft_parsing_fdf(char *str, int *tab, t_map **map)
{
	int		fd;

	map = NULL;
	fd = open(str, O_RDONLY);
	if (fd < 1)
		return (NULL);
	if (!ft_first_check(fd, tab))
		return (NULL);
	close(fd);
	if (!(map = ft_init_map(tab[0], tab[1])))
		return (NULL);
	fd = open(str, O_RDONLY);
	if (fd < 1)
		return (0);
	map = ft_fill_map(map, fd);
	close(fd);
	return (map);
}
