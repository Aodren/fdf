/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_expose.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/15 16:51:33 by abary             #+#    #+#             */
/*   Updated: 2016/03/15 17:09:56 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"

int		ft_expose(t_mlx *e)
{

	mlx_put_image_to_window(e->mlx, e->win, e->img, e->winx / 2- e->imgx / 2,
			e->winy / 2  - e->imgy / 2);
	mlx_destroy_image(e->mlx, e->img);




	return (1);
}
