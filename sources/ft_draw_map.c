/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/06 01:07:57 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:47:21 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"
#include "libft.h"
#include <math.h>

static void		ft_create_img(t_mlx *e)
{
	int bits;
	int size;
	int endian;

	e->img = mlx_new_image(e->mlx, e->imgx, e->imgy);
	e->addr = (int *)mlx_get_data_addr(e->img, &bits, &size, &endian);
}


static void		ft_destroy_img(t_mlx *e)
{
	mlx_put_image_to_window(e->mlx, e->win, e->img, e->winx / 2 - e->imgx / 2,
			e->winy / 2 - e->imgy / 2);
	mlx_destroy_image(e->mlx, e->img);
}

static	void	ft_calcul_coord(t_mlx *e)
{
	ft_copy_map(e);
	ft_translate(e);
	ft_rotate_z(e);
	ft_rotate_x(e);
	ft_rotate_y(e);
}

static	void	ft_draw_1(t_mlx *e, int i, int j, int cor[4])
{
	cor[X2] = e->mapc[i][j + 1].x + e->decalx;
	cor[Y2] = e->mapc[i][j + 1].y + e->decaly;
	ft_draw_line(cor[X1], cor[X2], cor[Y1], cor[Y2],
			e, e->map[i][j + 1].color);
}

static	void	ft_draw_2(t_mlx *e, int i, int j, int cor[4])
{
	cor[X2] = e->mapc[i + 1][j].x + e->decalx;
	cor[Y2] = e->mapc[i + 1][j].y + e->decaly;
	ft_draw_line(cor[X1], cor[X2], cor[Y1], cor[Y2],
			e, e->map[i +1][j].color);
}

int				ft_draw_map(t_mlx *e)
{
	int i;
	int j;
	int	cor[4];

	ft_create_img(e);
	i = 0;
	while (i < e->ligne)
	{
		j = 0;
		while (j < e->colonne)
		{
			ft_calcul_coord(e);
			cor[X1] = e->mapc[i][j].x + e->decalx;
			cor[Y1] = e->mapc[i][j].y + e->decaly;
			if (j + 1 < e->colonne)
				ft_draw_1(e, i, j, cor);
			if (i + 1 < e->ligne)
				ft_draw_2(e, i, j, cor);
			++j;
		}
		++i;
	}
	ft_destroy_img(e);
	return (1);
}
