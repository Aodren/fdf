/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/06 05:06:31 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 13:23:19 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#include "fdf.h"
#include "libft.h"

void ft_line(int x0, int y0, int x1, int y1, t_mlx *e, int color)
{

	int dx = ft_abs(x1-x0), sx = x0<x1 ? 1 : -1;
	int dy = ft_abs(y1-y0), sy = y0<y1 ? 1 : -1;
	int err = (dx>dy ? dx : -dy)/2, e2;

	while(1)
	{
		if ((x0 + e->imgy * y0)  <= e->imgx * e->imgy &&
		(x0 + e->imgy * y0) >= 0)
			e->addr[x0 + e->imgy * y0] = color;
		if (x0==x1 && y0==y1)
			break;
		e2 = err;
		if (e2 >-dx) { err -= dy; x0 += sx; }
		if (e2 < dy) { err += dx; y0 += sy; }
	}
}


void ft_draw_line(int x1, int x2, int y1, int y2, t_mlx *e, int color)
{
	ft_line(x1, y1, x2, y2, e, color);
}
