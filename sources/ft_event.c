/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_event.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/06 01:12:06 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:28:24 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "mlx.h"
#include "fdf.h"
#include <math.h>

void		ft_quit(void)
{
	exit(EXIT_SUCCESS);
}

void		ft_norm_event(int keycode, t_mlx *e)
{
	if (keycode == 12)
		ft_increase_z(e);
	else if (keycode == 13)
		ft_decrease_z(e);
	else if (keycode == 126)
		--e->decaly;
	else if (keycode == 125)
		++e->decaly;
	else if (keycode == 124)
		++e->decalx;
	else if (keycode == 123)
		--e->decalx;
	else if (keycode == 37)
		ft_rotate_y_360(e);
	else if (keycode == 40)
		ft_rotate_x_360(e);
	else if (keycode == 38)
		ft_rotate_z_360(e);
}

int			ft_front(int keycode, t_mlx *e)
{
	if (keycode == 53)
		ft_quit();
	else if (keycode == 78)
		ft_img_minus(e);
	else if (keycode == 69)
		ft_img_plus(e);
	else if (keycode == 7)
		e->rotx = ++e->angl_x * M_PI / 180;
	else if (keycode == 1)
		e->rotx = --e->angl_x * M_PI / 180;
	else if (keycode == 16)
		e->roty = ++e->angl_y * M_PI / 180;
	else if (keycode == 32)
		e->roty = --e->angl_y * M_PI / 180;
	else if (keycode == 6)
		e->rotz = ++e->angl_z * M_PI / 180;
	else if (keycode == 0)
		e->roty = --e->angl_z * M_PI / 180;
	else
		ft_norm_event(keycode, e);
	ft_draw_map(e);
	return (1);
}
