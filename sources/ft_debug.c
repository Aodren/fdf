/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_debug.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 20:10:12 by abary             #+#    #+#             */
/*   Updated: 2016/03/12 20:57:29 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libft.h"

void	ft_print_map(t_map **map, int *tab)
{
	int i;
	int j;

	i = 0;
	while (i < tab[0])
	{
		j = 0;
		while (j < tab[1])
		{

			ft_printf("z : %d, y : %d, x : %d, color %d\n", map[i][j].z,
					map[i][j].y, map[i][j].x, map[i][j].color);

			++j;
		}
		++i;
	}
}
