/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 15:28:53 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:12:32 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fdf.h"
#include "mlx.h"
#include <stdlib.h>

int		main(int argc, char **argv)
{
	t_mlx	*e;
	int		tab[2];

	if (argc != 2)
	{
		ft_putendl("INVALID ARG");
		return (0);
	}
	else
	{
		e = ft_memalloc(sizeof(t_mlx));
		e->map = NULL;
		e->mlx = NULL;
		e->win = NULL;
		if (!(e->map = ft_parsing_fdf(argv[1], tab, e->map)))
		{
			ft_printf("ERROR\n");
			exit(EXIT_SUCCESS);
		}
		e->ligne = tab[0];
		e->colonne = tab[1];
		ft_fdf(e);
	}
	exit(EXIT_SUCCESS);
	return (0);
}
