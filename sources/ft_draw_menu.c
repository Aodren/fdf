/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw_menu.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/19 12:28:47 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:30:02 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "mlx.h"

int		ft_draw_menu(t_mlx *e)
{
	mlx_string_put(e->mlx, e->win, 0, 5, 0xFFFFFF, "/-------FDF------\\");
	mlx_string_put(e->mlx, e->win, 0, 15, 0xFFFFFF, "|KEY       ACTION|");
	mlx_string_put(e->mlx, e->win, 0, 30, 0xFFFFFF, "|->         RIGTH|");
	mlx_string_put(e->mlx, e->win, 0, 44, 0xFFFFFF, "|^               |");
	mlx_string_put(e->mlx, e->win, 0, 44, 0xFFFFFF, "||             UP|");
	mlx_string_put(e->mlx, e->win, 0, 60, 0xFFFFFF, "||               |");
	mlx_string_put(e->mlx, e->win, 0, 65, 0xFFFFFF, "|v           DOWN|");
	mlx_string_put(e->mlx, e->win, 0, 80, 0xFFFFFF, "|->            UP|");
	mlx_string_put(e->mlx, e->win, 0, 95, 0xFFFFFF, "|+        ZOOM IN|");
	mlx_string_put(e->mlx, e->win, 0, 110, 0xFFFFFF, "|-       ZOOM OUT|");
	mlx_string_put(e->mlx, e->win, 0, 125, 0xFFFFFF, "|x     ROTATE X +|");
	mlx_string_put(e->mlx, e->win, 0, 140, 0xFFFFFF, "|s     ROTATE X -|");
	mlx_string_put(e->mlx, e->win, 0, 155, 0xFFFFFF, "|y     ROTATE Y +|");
	mlx_string_put(e->mlx, e->win, 0, 170, 0xFFFFFF, "|u     ROTATE Y -|");
	mlx_string_put(e->mlx, e->win, 0, 185, 0xFFFFFF, "|z     ROTATE Z +|");
	mlx_string_put(e->mlx, e->win, 0, 200, 0xFFFFFF, "|a     ROTATE Z -|");
	mlx_string_put(e->mlx, e->win, 0, 215, 0xFFFFFF, "|w        DEPTH -|");
	mlx_string_put(e->mlx, e->win, 0, 230, 0xFFFFFF, "|q        DEPTH +|");
	mlx_string_put(e->mlx, e->win, 0, 245, 0xFFFFFF, "|l          180 Y|");
	mlx_string_put(e->mlx, e->win, 0, 260, 0xFFFFFF, "|k          180 X|");
	mlx_string_put(e->mlx, e->win, 0, 275, 0xFFFFFF, "|j          180 Z|");
	mlx_string_put(e->mlx, e->win, 0, 285, 0xFFFFFF, "\\----------------/");
	return (1);
}
