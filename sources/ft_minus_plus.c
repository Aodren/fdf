/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minus_plus.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/14 15:07:04 by abary             #+#    #+#             */
/*   Updated: 2016/03/18 21:15:58 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libft.h"
void	ft_img_minus(t_mlx *e)
{
	int i;
	int j;
	int spacey;
	int spacex;

	if (e->space)
	{
		spacey = 0;
		i = 0;
		while (i < e->ligne)
		{
			j = 0;
			spacex = 0;
			while (j < e->colonne)
			{
				e->map[i][j].x += spacex;
				e->map[i][j].y += spacey;
				--spacex;
				++j;
			}
			--spacey;
			++i;
		}
		e->space--;
	}

}

void	ft_img_plus(t_mlx *e)
{
	int i;
	int j;
	int spacey;
	int spacex;

	e->space++;
	spacey = 0;
	i = 0;
	while (i < e->ligne)
	{
		j = 0;
		spacex = 0;
		while (j < e->colonne)
		{
			e->map[i][j].x += spacex;
			e->map[i][j].y += spacey;
			++spacex;
			++j;
		}
		++spacey;
		++i;
	}
}

void		ft_increase_z(t_mlx *e)
{
	int i;
	int j;

	i = 0;
	j = 0;
	//e->z += 0.1;
	while (i < e->ligne)
	{
		j = 0;
		while (j < e->colonne)
		{
			if (e->map[i][j].z >= 10)
				e->map[i][j].z *= e->z;
			else
				e->map[i][j].z *= 2;
			++j;
		}
		++i;
	}
}

void		ft_decrease_z(t_mlx *e)
{
	int i;
	int j;

	i = 0;
	j = 0;
	//e->z -= 0.1;
	while (i < e->ligne)
	{
		j = 0;
		while (j < e->colonne)
		{
			if (e->map[i][j].z > 1 || e->map[i][j].z < -1)
				e->map[i][j].z /= e->z;
			++j;
		}
		++i;
	}
}
