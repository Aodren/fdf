/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calcul_matrice.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/12 19:14:51 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 13:34:52 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libft.h"
#include <math.h>

void	ft_rotate_x(t_mlx *e)
{
	int i;
	int	j;
	int y;
	int z;

	i = 0;
	while (i < e->ligne)
	{
		j = 0;
		y = e->mapc[i][j].y;
		while (j < e->colonne)
		{
			z = e->mapc[i][j].z;
			e->mapc[i][j].y = y * cos(e->rotx) - (z * sin(e->rotx));
			e->mapc[i][j].z = y * sin(e->rotx) + (z * cos(e->rotx));
			++j;
		}
		++i;
	}
}

void	ft_rotate_y(t_mlx *e)
{
	int i;
	int	j;
	int x;
	int z;

	i = 0;
	while (i < e->ligne)
	{
		j = 0;
		while (j < e->colonne)
		{
			x = e->mapc[i][j].x;
			z = e->mapc[i][j].z;
			e->mapc[i][j].x = x * cos(e->roty) + (z * sin(e->roty));
			e->mapc[i][j].z = x * -sin(e->roty) + (z * cos(e->roty));
			++j;
		}
		++i;
	}
}

void	ft_rotate_z(t_mlx *e)
{
	int i;
	int	j;
	int x;
	int y;

	i = 0;
	while (i < e->ligne)
	{
		j = 0;
		y = e->mapc[i][j].y;
		while (j < e->colonne)
		{
			x = e->mapc[i][j].x;
			e->mapc[i][j].x = x * cos(e->rotz) - (y * sin(e->rotz));
			e->mapc[i][j].y = x * sin(e->rotz) + (y * cos(e->rotz));
			++j;
		}
		++i;
	}
}

void	ft_copy_map(t_mlx *e)
{
	int i;
	int j;

	i = 0;
	e->maxx = 0;
	e->maxy = 0;
	e->maxz = 0;
	while (i < e->ligne)
	{
		j = 0;
		while (j < e->colonne)
		{
			e->mapc[i][j].x = e->map[i][j].x;
			if (e->mapc[i][j].x > e->maxx)
				e->maxx = e->mapc[i][j].x;
			e->mapc[i][j].y = e->map[i][j].y;
			if (e->mapc[i][j].y > e->maxy)
				e->maxy = e->mapc[i][j].y;
			e->mapc[i][j].z = e->map[i][j].z;
			if (e->mapc[i][j].z > e->maxz)
				e->maxz = e->mapc[i][j].z;
			++j;
		}
		++i;
	}
}

void	ft_translate(t_mlx *e)
{
	int		i;
	int		j;
	int		translatex;
	int		translatey;
	int		translatez;

	i = 0;
	translatey = -(e->maxy / 2);
	translatex = -(e->maxx / 2);
	translatez = -(e->maxz / 2);
	while (i < e->ligne)
	{
		j = 0;
		while (j < e->colonne)
		{
			e->mapc[i][j].x += translatex;
			e->mapc[i][j].y += translatey;
			e->mapc[i][j].z += translatez;
			++j;
		}
		++i;
	}
}
