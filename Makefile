# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: abary <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/23 12:31:57 by abary             #+#    #+#              #
#    Updated: 2016/03/19 14:24:38 by abary            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

INC_DIR = includes

LIB_DIR = libft

INC_LIB_DIR = $(LIB_DIR)/$(INC_DIR)

NAME_LIB = fdf.a

CFLAGS = -Wall -Werror -Wextra -I$(INC_DIR) -I $(INC_LIB_DIR)
#CFLAGS = -I$(INC_DIR) -I $(INC_LIB_DIR)

SRC = main.c ft_parsing_fdf.c ft_first_check.c ft_fill_map.c ft_fdf.c\
	  ft_draw_map.c ft_event.c ft_draw_line.c ft_calcul_matrice.c ft_debug.c\
	  ft_minus_plus.c ft_expose.c ft_init.c ft_draw_menu.c ft_rotate_360.c

SRCS = $(addprefix sources/,$(SRC))

MLX = -L minilibx_macos -lmlx -framework OpenGL -framework Appkit
OBJ = $(SRCS:.c=.o)
CC = gcc
all : $(NAME)

$(NAME) : $(OBJ)
	(cd $(LIB_DIR) && $(MAKE))
	ar -r $(NAME_LIB) $(OBJ)
#	clang-3.5 -o $(NAME) $(LIB_DIR)/libft.a $(NAME_LIB) libft/libft.a push_swap.a
#	gcc -o $(NAME) $(LIB_DIR)/libft.a $(NAME_LIB) libft/libft.a push_swap.a -fsanitize=address -g
	gcc -o $(NAME) $(NAME_LIB) libft/libft.a $(MLX)

clean :
	(cd $(LIB_DIR) && make clean && cd ..)
	rm -rf $(OBJ)

fclean : clean
	(cd $(LIB_DIR) && make fclean && cd ..)
	rm -rf $(NAME)
	rm -rf $(NAME_LIB)

re : fclean all

.PHONY: all clean flcean re
