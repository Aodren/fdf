/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 17:52:17 by abary             #+#    #+#             */
/*   Updated: 2016/03/19 14:39:01 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# define WINX 1800
# define WINY 1000
# define IMGX 1000
# define IMGY 1000
# define X1 0
# define X2 1
# define Y1 2
# define Y2 3
typedef struct s_map
{
	int		z;
	int 	y;
	int		x;
	int		color;
}				t_map;

typedef struct s_mlx
{
	void	*mlx;
	void	*win;
	void	*img;
	int		*addr;
	double	rotx;
	double	roty;
	double	rotz;
	int		keycode;
	int		angl_x;
	int		angl_y;
	int		angl_z;
	double	z;
	int		imgx;
	int		imgy;
	int		maxx;
	int		maxy;
	int		maxz;
	int		winx;
	int		winy;
	int		decalx;
	int		decaly;
	int		space;
	t_map	**map;
	t_map	**mapc;
	int		ligne;
	int		colonne;
}				t_mlx;
/*
*******************************************************************************
**								DEBUG										  *
*******************************************************************************
*/
void	ft_print_map(t_map **map, int *tab);
void	ft_copy_map(t_mlx *e);
/*
*******************************************************************************
**								PARSING										  *
*******************************************************************************
*/

t_map		**ft_parsing_fdf(char *str, int *tab, t_map **map);
int			ft_first_check(int fd, int *tab);
t_map		**ft_fill_map(t_map **map, int fd);

/*
*******************************************************************************
**								FDF 										  *
*******************************************************************************
*/
int			ft_fdf(t_mlx *e);

/*
*******************************************************************************
**								DRAW 										  *
*******************************************************************************
*/
int			ft_draw_map(t_mlx *e);
int			ft_draw_menu(t_mlx *e);
void		ft_draw_line(int x1, int x2, int y1, int y2, t_mlx *e, int color);
int			ft_expose(t_mlx *e);
/*
*******************************************************************************
**								EVENT 										  *
*******************************************************************************
*/
void		ft_quit(void);
void		ft_img_plus(t_mlx *e);
void		ft_img_minus(t_mlx *e);
int			ft_front(int keycode, t_mlx *e);
void		ft_increase_z(t_mlx *e);
void		ft_decrease_z(t_mlx *e);
void		ft_rotate_x_360(t_mlx *e);
void		ft_rotate_y_360(t_mlx *e);
void		ft_rotate_z_360(t_mlx *e);
/*
*******************************************************************************
**								 INIT										  *
*******************************************************************************
*/
t_map		**ft_init_map(int ligne, int col);
void		ft_init_coord(t_mlx * e);
/*
*******************************************************************************
**								CALCUL 										  *
*******************************************************************************
*/

void	ft_rotate_x(t_mlx *e);
void	ft_rotate_y(t_mlx *e);
void	ft_rotate_z(t_mlx *e);
void	ft_translate(t_mlx *e);
#endif
