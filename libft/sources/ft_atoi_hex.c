/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_hex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/05 17:01:34 by abary             #+#    #+#             */
/*   Updated: 2016/03/05 20:39:10 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int ft_puissance(int hexa, int expo)
{
	int nbr;

	nbr = hexa;
	if (expo == 0)
		return (1);
	if (expo == 1)
		return hexa;
	while (expo > 1)
	{
		hexa *= nbr;
		expo--;
	}
	return (hexa);
}

int		ft_atoi_hex(const char *str)
{
	int nb;
	int hex;
	int expo;

	hex = 16;
	expo = 0;
	nb = 0;
	while (*str == '\t' || *str == '\n' || *str == '\v'
			|| *str == '\f' || *str == '\r' || *str == ' ')
		++str;
	if (*str == '0' && (*(str + 1) == 'X' || *(str + 1) == 'x'))
		str += 2;
	else
		return (0);
	while (*str && *str != ' ')
		++str;
	--str;
	while (*str != 'X' && *str != 'x')
	{
		if (*str >= 48 && *str <= 57)
			nb += (*str - 48) * ft_puissance(hex, expo);
		else if (*str >= 65 && *str <= 70)
			nb += (*str - 55 ) * ft_puissance(hex, expo);
		else if (*str >= 97 && *str <= 102)
			nb += (*str - 87) * ft_puissance(hex, expo);
		++expo;
		--str;
	}
	return (nb);
}
